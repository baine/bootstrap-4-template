
requirejs.config({
    baseUrl: './scripts/lib',
    shim: {
        bootstrap: {
            deps: [ 'jquery', 'tether' ]
        }
    },
    paths: {
        app: '../app',
        jquery: [ 'jquery-3.1.1.min' ],
        tether: [ '../app/tether-helper' ],
        bootstrap: [ 'bootstrap.min' ]
    },
    deps: ['bootstrap']
});

require(['jquery', 'app/message'], function($, message) {
    $(function() {
        $('body').append($('<p>').html(message));
    });
});
