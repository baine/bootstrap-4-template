
define(['tether.min', 'jquery'], function(tether, $) {
    window.Tether = tether;
    return tether;
});
